using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : PooledMonoBehaviour
{
    [SerializeField] WeaponTypeSO weaponType;
    private float moveSpeed;
    private float projectileTimerMax;

    private Rigidbody rb;

    private void Awake()
    {
        moveSpeed = weaponType.projectileSpeed;
        projectileTimerMax = weaponType.projectileTimerMax;
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.GetComponent<HealthSystem>().Damage(weaponType.damage);
            ResetProjectileTimer();
            ReturnToPool();
        }

        Obstacle obstacle = other.GetComponent<Obstacle>();
        if(obstacle != null)
        {
            ResetProjectileTimer();
            ReturnToPool();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = transform.forward * weaponType.projectileSpeed;

        projectileTimerMax -= Time.deltaTime;
        if (projectileTimerMax < 0)
        {
            ResetProjectileTimer();
            ReturnToPool();
        }
    }

    private void ResetProjectileTimer()
    {
        projectileTimerMax = weaponType.projectileTimerMax;
    }
}
