using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : PooledMonoBehaviour
{
    public static Enemy Create(Vector3 position)
    {
        Enemy pfEnemy = Resources.Load<Enemy>("pfEnemy");
        //Transform enemyTransform = Instantiate(pfEnemy, position, Quaternion.identity);


        Enemy enemy = pfEnemy.Get<Enemy>(position, Quaternion.identity);
        return enemy;
    }

    [SerializeField] EnemyTypeSO enemyType;
    
    public bool IsVisible { get; set; }
    public bool IsDead { get; private set; }
    private float visibilityTimer;
    private float visibilityTimerMax = 1f;
    private Transform targetTransform;
    private HealthSystem healthSystem;
    private float lookForTargetTimer;
    private float lookForTargetTimerMax = .2f;

    private Rigidbody rb;
    private NavMeshAgent navMeshAgent;

    private void Start()
    {
        if(BuildingManager.Instance.GetHQBuilding() != null)
            targetTransform = BuildingManager.Instance.GetHQBuilding().transform;

        healthSystem = GetComponent<HealthSystem>();
        rb = GetComponent<Rigidbody>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        float moveSpeedPerWave = enemyType.moveSpeed + (EnemyWaveManager.Instance.GetWaveNumber() * 0.2f);
        navMeshAgent.speed = Mathf.Clamp(moveSpeedPerWave, 0f, 10f);

        healthSystem.OnDamaged += HealthSystem_OnDamaged;
        healthSystem.OnDied += HealthSystem_OnDied;

        lookForTargetTimer = Random.Range(0f, lookForTargetTimerMax);
    }

    private void OnEnable()
    {
        StartCoroutine(EnemyIsAlive());
    }

    private IEnumerator EnemyIsAlive()
    {
        yield return new WaitForSeconds(1f);
        IsDead = false;
    }

    private void HealthSystem_OnDamaged()
    {
        SoundManager.Instance.PlaySound(SoundManager.Sound.EnemyHit);
    }

    private void HealthSystem_OnDied()
    {
        SoundManager.Instance.PlaySound(SoundManager.Sound.EnemyDie);
        Destroy(gameObject);
    }

    private void Update()
    {
        HandleVisibility();
        HandleMovement();
        HandleTargetting();
    }

    private void HandleVisibility()
    {
        if (IsVisible)
        {
            visibilityTimer -= Time.deltaTime;
            if (visibilityTimer < 0)
            {
                visibilityTimer = visibilityTimerMax;
                IsVisible = false;
            }
        }
    }

    private void HandleTargetting()
    {
        lookForTargetTimer -= Time.deltaTime;
        if (lookForTargetTimer < 0f)
        {
            lookForTargetTimer += lookForTargetTimerMax;
            LookForTargets();
        }
    }

    private void HandleMovement()
    {
        if (targetTransform != null)
        {
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(targetTransform.position);
        }
        else
        {
            navMeshAgent.isStopped = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        IDamagable damagable = collision.collider.GetComponent<IDamagable>();
        if(damagable != null)
        {
            GameObject objectToDamage = collision.collider.gameObject;
            DamagableUnit(objectToDamage);
        }
    }

    private void DamagableUnit(GameObject objectToDamage)
    {
        //HAVE COLLIDED WITH IDAMAGABLE UNITS
        HealthSystem healthSystem = objectToDamage.GetComponent<HealthSystem>();
        healthSystem.Damage(enemyType.damage);
        IsVisible = false;
        Died();
    }

    public void Died()
    {
        healthSystem.SetHealthAmountMax(enemyType.healthAmountMax, true);
        IsDead = true;
        SpawnDeathParticleFX();
        ReturnToPool();
    }

    private void SpawnDeathParticleFX()
    {
        DeathImpact deathImpact = enemyType.particleOnDeath.deathImpact.Get<DeathImpact>(transform.position, Quaternion.identity);
        deathImpact.GetComponent<ParticleSystem>().Play();
    }

    private void LookForTargets()
    {
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, enemyType.detectionRadius);

        foreach(Collider collider in colliderArray)
        {
            IDamagable damagable = collider.GetComponent<IDamagable>();
            if(damagable != null)
            {
                GameObject objectToLocate = collider.gameObject;

                if(targetTransform == null)
                {
                    targetTransform = objectToLocate.transform;
                }
                else
                {
                    if (Vector3.Distance(transform.position, objectToLocate.transform.position) < Vector3.Distance(transform.position, targetTransform.position))
                    {
                        targetTransform = objectToLocate.transform;
                    }
                }
            }
            
        }

        if(targetTransform == null)
        {
            //No Targets found within range
            if(BuildingManager.Instance.GetHQBuilding() != null)
                targetTransform = BuildingManager.Instance.GetHQBuilding().transform;
        }
    }
}
