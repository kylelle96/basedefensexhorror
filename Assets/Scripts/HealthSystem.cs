using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HealthSystem : MonoBehaviour
{
    public event Action OnDamaged;
    public event Action OnDied;
    public event Action OnHealed;

    public UnityEvent OnDamagedEvent;
    public UnityEvent OnDiedEvent;
    public UnityEvent OnHealedEvent;

    public bool isAction = true;

    private int _healthAmount;
    [SerializeField] private int _healthAmountMax;


    private void Awake()
    {
        SetHealthAmountMax(_healthAmountMax, true);
    }


    public void Damage(int damageAmount)
    {
        _healthAmount -= damageAmount;

        _healthAmount = Mathf.Clamp(_healthAmount, 0, _healthAmountMax);

        if (isAction)
            OnDamaged?.Invoke();
        else
            OnDamagedEvent?.Invoke();

        if (isDead())
        {
            if (isAction)
                OnDied?.Invoke();
            else
                OnDiedEvent?.Invoke();
        }

    }

    public void Heal(int healAmount)
    {
        _healthAmount += healAmount;
        _healthAmount = Mathf.Clamp(_healthAmount, 0, _healthAmountMax);

        if (isAction)
            OnHealed?.Invoke();
        else
            OnHealedEvent?.Invoke();

    }

    public void HealFull()
    {
        _healthAmount = _healthAmountMax;

        if (isAction)
            OnHealed?.Invoke();
        else
            OnHealedEvent?.Invoke();
    }

    public bool isDead()
    {
        return _healthAmount == 0;
    }

    public int GetHealthAmount()
    {
        return _healthAmount;
    }

    public int GetHealthAmountMax()
    {
        return _healthAmountMax;
    }

    public void SetHealthAmountMax(int healthAmountMax, bool updateHealthAmount)
    {
         _healthAmountMax = healthAmountMax;
        if (updateHealthAmount)
        {
            _healthAmount = healthAmountMax;
        }
    }

    public bool IsFullHealth()
    {
        return _healthAmount == _healthAmountMax;
    }

    public float GetHealthAmountNormalize()
    {
        return (float)_healthAmount / _healthAmountMax;
    }
}
