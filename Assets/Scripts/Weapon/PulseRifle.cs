using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseRifle : Weapon, IWeapon
{
    //WeaponTypeSO weaponType;
    //WeaponController weaponController;
    //private float shootTimer;
    //private float shootTimerMax;
    private int burstIndex = 3;

    private void Awake()
    {
        weaponType = GetComponent<WeaponTypeHolder>().weaponType;
        shootTimerMax = weaponType.shootTimerMax;
        weaponController = transform.parent.GetComponent<WeaponController>();

    }

    private void Start()
    {
        weaponController.OnUpdateWeapon += WeaponController_OnUpdateWeapon;
        CheckWeaponIsActive();
    }

    private void OnDestroy()
    {
        weaponController.OnUpdateWeapon -= WeaponController_OnUpdateWeapon;
    }

    private void WeaponController_OnUpdateWeapon()
    {
        CheckWeaponIsActive();
    }

    private void CheckWeaponIsActive()
    {
        if (weaponController.GetActiveWeaponType() == weaponType)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Update()
    {
        shootTimer -= Time.deltaTime;
        if (Input.GetMouseButton(0) && BuildingManager.Instance.GetActiveBuilingType() == null)
        {
            if (shootTimer < 0)
            {
                Shoot();
                shootTimer = shootTimerMax;
            }
        }

    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    public override void Shoot()
    {
        //base.Shoot();
        StartCoroutine(BurstFire());
    }

    public IEnumerator BurstFire()
    {
        int index = 0;
        while(index < burstIndex)
        {
            base.Shoot();
            yield return new WaitForSeconds(0.1f);
            weaponType.projectilePrefab.Get<Projectile>(transform.position, transform.rotation);
            index++;
        }
        
    }

}
