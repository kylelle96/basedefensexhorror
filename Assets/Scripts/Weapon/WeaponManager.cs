using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public event Action OnWeaponChanged;

    public static WeaponManager Instance { get; private set; }
    private WeaponTypeSO activeWeaponType;
    private int weaponIndex = 0;
    private Dictionary<int, WeaponTypeSO> weaponTypeDictionary;

    public WeaponTypeSO ActiveWeaponType { 
        get { return activeWeaponType; }
    }

    private void Awake()
    {
        Instance = this;
        weaponTypeDictionary = new Dictionary<int, WeaponTypeSO>();
        WeaponTypeListSO weaponTypeList = Resources.Load<WeaponTypeListSO>(typeof(WeaponTypeListSO).Name);

        int index = 0;
        foreach(WeaponTypeSO weaponType in weaponTypeList.list)
        {
            weaponTypeDictionary[index] = weaponType;
            index++;
        }

        activeWeaponType = weaponTypeDictionary[0];
    }

    private void Update()
    {
        ScrollWheelWeaponIndex(); // updates weapon index via scroll wheel
        NumKeyWeaponIndex(); // update weapon index via number keys (KeyCode.Alpha1... etc etc)
        UpdateActiveWeapon(weaponIndex);
    }

    private void NumKeyWeaponIndex()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            
            UpdateWeaponIndex(0);
            BuildingManager.Instance.SetActiveBuildingType(null);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            UpdateWeaponIndex(1);
            BuildingManager.Instance.SetActiveBuildingType(null);
        }

 
    }

    private void ScrollWheelWeaponIndex()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (weaponIndex >= weaponTypeDictionary.Count - 1)
            {
                weaponIndex = 0;
                //BuildingManager.Instance.SetActiveBuildingType(null);
                OnWeaponChanged?.Invoke();
            }
            else
            {
                weaponIndex++;
                //BuildingManager.Instance.SetActiveBuildingType(null);
                OnWeaponChanged?.Invoke();
            }

        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (weaponIndex <= 0)
            {
                weaponIndex = weaponTypeDictionary.Count - 1;
                //BuildingManager.Instance.SetActiveBuildingType(null);
                OnWeaponChanged?.Invoke();
            }
            else
            {
                weaponIndex--;
                //BuildingManager.Instance.SetActiveBuildingType(null);
                OnWeaponChanged?.Invoke();
            }
        }
    }

    private void UpdateWeaponIndex(int index)
    {
        weaponIndex = index;
        OnWeaponChanged?.Invoke();
    }

    private void UpdateActiveWeapon(int index)
    {
        activeWeaponType = weaponTypeDictionary[index];
    }
}
