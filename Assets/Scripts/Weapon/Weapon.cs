using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Weapon : MonoBehaviour, IWeapon
{
    public UnityEvent OnShootEvent;

    protected WeaponTypeSO weaponType;
    protected WeaponController weaponController;
    protected float shootTimer;
    protected float shootTimerMax;



    public virtual void Shoot()
    {
        OnShootEvent?.Invoke();
    }

    private void Awake()
    {
        weaponType = GetComponent<WeaponTypeHolder>().weaponType;
        shootTimerMax = weaponType.shootTimerMax;
        weaponController = transform.parent.GetComponent<WeaponController>();
    }
}
