using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/ProjectileImpact")]
public class ProjectileImpactSO : ScriptableObject
{
    public ProjectileImpact projectileImpact;
}
