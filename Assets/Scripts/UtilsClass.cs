using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilsClass
{
    private static Camera mainCamera;
    private static LayerMask groundMask = LayerMask.GetMask("Ground");

    public static Vector3 GetMouseWorldPosition2D()
    {
        if (mainCamera == null) mainCamera = Camera.main;

        Vector3 mouseWorldPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mouseWorldPosition.y = 0;

        return mouseWorldPosition;
    }

    public static Vector3 GetMouseWorldPosition3D()
    {
        if (mainCamera == null) mainCamera = Camera.main;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out RaycastHit hit, float.MaxValue, groundMask);


        return hit.point;
    }


    public static Vector3 GetRandomDir()
    {
        return new Vector3(
            Random.Range(-1f, 1f),
            0,
            Random.Range(-1f, 1f)).normalized;
    }

    public static float GetAngleFromVector(Vector3 vector)
    {
        float radians = Mathf.Atan2(vector.z, vector.x);
        float degrees = radians * Mathf.Rad2Deg;
        return degrees;
    }
}
