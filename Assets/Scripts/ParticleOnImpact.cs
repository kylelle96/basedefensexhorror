using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleOnImpact : MonoBehaviour
{
    [SerializeField] ProjectileImpactSO projectileImpactType;

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            ProjectileImpact projectileImpact = projectileImpactType.projectileImpact;
            projectileImpact.Get<ProjectileImpact>(transform.position, Quaternion.identity);
        }
        Obstacle obstacle = other.GetComponent<Obstacle>();
        if (obstacle != null)
        {
            ProjectileImpact projectileImpact = projectileImpactType.projectileImpact;
            projectileImpact.Get<ProjectileImpact>(transform.position, Quaternion.identity);
        }

    }



}
