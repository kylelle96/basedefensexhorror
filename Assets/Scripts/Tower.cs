using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Tower : MonoBehaviour
{
    public UnityEvent OnShootEvent;

    [SerializeField] private float shootTimerMax;

    private float shootTimer;
    private BuildingTypeSO buildingType;
    private Enemy targetEnemy;
    private float lookForTargetTimer;
    private float lookForTargetTimerMax = .2f;
    private Vector3 projectileSpawnPosition;

    private void Awake()
    {
        buildingType = GetComponent<BuildingTypeHolder>().buildingType;
        projectileSpawnPosition = transform.Find("projectileSpawnPosition").position;
    }

    private void Update()
    {
        if (targetEnemy != null) {
            if (targetEnemy.IsDead) {
                targetEnemy = null;
            }
        }                           
        HandleTargetting();
        HandleShooting();
    }

    private void HandleTargetting()
    {
        lookForTargetTimer -= Time.deltaTime;
        if (lookForTargetTimer < 0f)
        {
            lookForTargetTimer += lookForTargetTimerMax;
            LookForTargets();
        }
    }

    private void HandleShooting()
    {
        shootTimer -= Time.deltaTime;
        if(shootTimer < 0)
        {
            shootTimer += shootTimerMax;
            if (targetEnemy != null && targetEnemy.IsVisible)
            {
                OnShootEvent?.Invoke();
                TowerProjectile.Create(projectileSpawnPosition, targetEnemy);
            }
        }
       
    }

    private void LookForTargets()
    {
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, buildingType.detectionRadius);

        foreach (Collider collider in colliderArray)
        {
            Enemy enemy = collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                if (enemy.IsVisible == false) continue;

                if (targetEnemy == null)
                {
                    targetEnemy = enemy;
                }
                else
                {

                    if (Vector3.Distance(transform.position, enemy.transform.position) < 
                        Vector3.Distance(transform.position, targetEnemy.transform.position))
                    {
                        targetEnemy = enemy;
                    }
                }
            }

        }
    }
}
