using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerProjectile : PooledMonoBehaviour
{
    public static TowerProjectile Create(Vector3 position, Enemy enemy)
    {
        TowerProjectile pfTowerProjectile = Resources.Load<TowerProjectile>("pfTowerProjectile");
        //Transform towerProjectileTransform = Instantiate(pfTowerProjectile, position, Quaternion.identity);
  
        TowerProjectile towerProjectile = pfTowerProjectile.Get<TowerProjectile>(position, Quaternion.identity);
        towerProjectile.SetTargetEnemy(enemy);

        return towerProjectile;
    }


    [SerializeField] float moveSpeed = 20f;
    private Enemy targetEnemy;
    private Vector3 lastMoveDirection;
    private float timeToDie = 2f;

    private void Update()
    {
        Vector3 moveDir;
        if(targetEnemy.gameObject.activeSelf)
        {
            moveDir = (targetEnemy.transform.position - transform.position).normalized;
            transform.LookAt(targetEnemy.transform.position + Vector3.up * transform.position.y);
            lastMoveDirection = moveDir;
        }
        else
        {
            moveDir = lastMoveDirection;
        }
        transform.position += moveDir * moveSpeed * Time.deltaTime;
        //transform.eulerAngles = new Vector3(UtilsClass.GetAngleFromVector(targetEnemy.transform.position), 0, 0);
        

        timeToDie -= Time.deltaTime;
        if(timeToDie < 0)
        {
            timeToDie = 2f;
            ReturnToPool();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if(enemy != null)
        {
            enemy.GetComponent<HealthSystem>().Damage(10);
            timeToDie = 2f;
            ReturnToPool();
        }
        Obstacle obstacle = other.GetComponent<Obstacle>();
        if(obstacle != null)
        {
            timeToDie = 2f;
            ReturnToPool();
        }
    }

    private void SetTargetEnemy(Enemy targetEnemy)
    {
        this.targetEnemy = targetEnemy;
    }
}
