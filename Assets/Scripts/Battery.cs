using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{
    [SerializeField] private float maxViewRadius = 15f;
    //[SerializeField] private float maxViewAngle = 90f;
    [SerializeField] private float rechargeSpeed = 2f;
    [SerializeField] private float batteryUsageSpeed = 1.5f;
    [SerializeField] private float batteryChargeRadius;
    private Transform activeRechargeStationTransform;
    private FieldOfView fieldOfView;
    private bool withinHQ;

    private void Awake()
    {
        fieldOfView = GetComponent<FieldOfView>();
    }

    private void Update()
    {
        LookForRechargeArea();
        ShouldRechargeBattery();
    }

    private void ShouldRechargeBattery()
    {
        if (activeRechargeStationTransform != null)
        {
            batteryChargeRadius = activeRechargeStationTransform.GetComponent<BuildingTypeHolder>().buildingType.batteryChargeRadius;
            float distanceToRechargeStation = Vector3.Distance(activeRechargeStationTransform.position, transform.position);
            if (distanceToRechargeStation < batteryChargeRadius)
            {
                RechargeFlashLight();
            }
            else
            {
                BatteryUsage();
            }
        }
        else
        {
            activeRechargeStationTransform = null;
            BatteryUsage();
        }
    }

    private void LookForRechargeArea()
    {
        Collider[] colliderArray = Physics.OverlapSphere(transform.position, batteryChargeRadius);
        foreach (Collider collider in colliderArray)
        {
            Building building = collider.GetComponent<Building>();

            if (building != null)
            {
                BuildingTypeHolder buildingTypeHolder = building.GetComponent<BuildingTypeHolder>();
                if (activeRechargeStationTransform == null)
                {
                    if (buildingTypeHolder.buildingType.canRecharge)
                    {
                        activeRechargeStationTransform = building.transform;
                    }
                }
                else
                {
                    if (Vector3.Distance(transform.position, building.transform.position) <
                        Vector3.Distance(transform.position, activeRechargeStationTransform.transform.position))
                    {
                        if (buildingTypeHolder.buildingType.canRecharge)
                        {
                            activeRechargeStationTransform = building.transform;
                        }
                    }
                }
            }

        }
    }

    private void BatteryUsage()
    {
        fieldOfView.ViewRadius -= batteryUsageSpeed * Time.deltaTime;
        //fieldOfView.ViewAngle -= batteryUsageSpeed * 5f * Time.deltaTime;
        fieldOfView.ViewRadius = Mathf.Clamp(fieldOfView.ViewRadius, 5, maxViewRadius);
        //fieldOfView.ViewAngle = Mathf.Clamp(fieldOfView.ViewAngle, 0, maxViewAngle);
    }

    private void RechargeFlashLight()
    {
        fieldOfView.ViewRadius += rechargeSpeed * Time.deltaTime;
        //fieldOfView.ViewAngle += rechargeSpeed * 5f * Time.deltaTime;
        fieldOfView.ViewRadius = Mathf.Clamp(fieldOfView.ViewRadius, 5, maxViewRadius);
        //fieldOfView.ViewAngle = Mathf.Clamp(fieldOfView.ViewAngle, 0, maxViewAngle);
    }
}
