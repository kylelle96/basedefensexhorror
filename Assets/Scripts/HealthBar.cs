using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private HealthSystem healthSystem;

    private Transform barTransform;

    private void Awake()
    {
        barTransform = transform.Find("bar");
    }

    private void Start()
    {
        UpdateBar();
        UpdateHealthBarVisible();
        healthSystem.OnDamagedEvent.AddListener(HealthSystem_OnDamaged);
        healthSystem.OnHealedEvent.AddListener(HealthSystem_OnHealed);
    }

    private void HealthSystem_OnHealed()
    {
        UpdateBar();
        UpdateHealthBarVisible();
    }

    private void HealthSystem_OnDamaged()
    {
        UpdateBar();
        UpdateHealthBarVisible();
    }

    public void UpdateBar()
    {
        barTransform.localScale = new Vector3(healthSystem.GetHealthAmountNormalize(), 1, 1);
    }

    public void UpdateHealthBarVisible()
    {
        if (healthSystem.IsFullHealth())
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }
}
