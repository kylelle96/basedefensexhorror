using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    public static GameOverUI Instance { get; private set; }
    [SerializeField] IntVariable _waveNumber;

    private void Awake()
    {
        Instance = this;

        transform.Find("retryButton").GetComponent<Button>().onClick.AddListener(()=> {
            GameSceneManager.Load(GameSceneManager.Scene.GameScene);
            Time.timeScale = 1;
        } );

        transform.Find("mainMenuButton").GetComponent<Button>().onClick.AddListener(() => {
            GameSceneManager.Load(GameSceneManager.Scene.MainMenuScene);
            Time.timeScale = 1;
        });

        Hide();
    }

    public void Show()
    {
        gameObject.SetActive(true);
        transform.Find("waveSurvivedText").GetComponent<TextMeshProUGUI>().SetText("You Survived " + _waveNumber.Value + " Waves!");
        Time.timeScale = 0;
    }

    private void Hide()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1;
    }
}
