using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EnemyWaveUI : MonoBehaviour
{
    public static EnemyWaveUI Instance { get; private set; }

    [SerializeField] private EnemyWaveManager enemyWaveManager;
    [SerializeField] private IntVariable _waveNumber;
    [SerializeField] private bool _resetWaves = true;
    private TextMeshProUGUI waveNumberText;
    private TextMeshProUGUI waveMessageText;
    private RectTransform enemyWaveSpawnPositionIndicator;
    private RectTransform closestEnemyPositionIndicator;
    private Camera mainCamera;

    private void Awake()
    {
        Instance = this;
        waveNumberText = transform.Find("waveNumberText").GetComponent<TextMeshProUGUI>();
        waveMessageText = transform.Find("waveMessageText").GetComponent<TextMeshProUGUI>();
        enemyWaveSpawnPositionIndicator = transform.Find("enemyWaveSpawnPositionIndicator").GetComponent<RectTransform>();
        closestEnemyPositionIndicator = transform.Find("closestEnemyPositionIndicator").GetComponent<RectTransform>();
        mainCamera = Camera.main;
    }
    private void Start()
    {
        enemyWaveManager.OnWaveNumberChanged += EnemyWaveManager_OnWaveNumberChanged;

        if (_resetWaves)
            _waveNumber.SetValue(0);

        SetWaveNumberText("Wave " + _waveNumber.Value);
    }

    private void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void EnemyWaveManager_OnWaveNumberChanged()
    {
        SetWaveNumberText("Wave " + _waveNumber.Value);
    }

    private void Update()
    {
        HandleNextWaveMessage();
        HandleNextWaveSpawnPositionIndicator();
        HandleClosestEnemyPositionIndicator();


    }

    private void HandleNextWaveMessage()
    {
        float nextWaveSpawnTimer = enemyWaveManager.GetNextWaveSpawnTimer();
        if (nextWaveSpawnTimer <= 0f)
        {
            SetMessageText("");
        }
        else
        {
            SetMessageText("Next Wave in " + nextWaveSpawnTimer.ToString("F1") + "s");
        }
    }

    private void HandleNextWaveSpawnPositionIndicator()
    {
        Vector3 dirToNextSpawnPosition = (enemyWaveManager.GetSpawnPosition() - mainCamera.transform.position).normalized;
        enemyWaveSpawnPositionIndicator.eulerAngles = new Vector3(0, 0, UtilsClass.GetAngleFromVector(dirToNextSpawnPosition));


        float distanceToNextSpawnPosition = Vector3.Distance(enemyWaveManager.GetSpawnPosition(), PlayerMovement.Instance.transform.position); //Change this to not use Controller Instance
        enemyWaveSpawnPositionIndicator.gameObject.SetActive(Mathf.Abs(distanceToNextSpawnPosition) > mainCamera.orthographicSize * 1.5f);
    }

    private void HandleClosestEnemyPositionIndicator()
    {
        float targetMaxRadius = 9999f;
        Collider[] colliderArray = Physics.OverlapSphere(PlayerMovement.Instance.transform.position, targetMaxRadius);

        Enemy targetEnemy = null;

        foreach (Collider collider in colliderArray)
        {
            Enemy enemy = collider.GetComponent<Enemy>();
            if (enemy != null)
            {
                if (targetEnemy == null)
                {
                    targetEnemy = enemy;
                }
                else
                {
                    if (Vector3.Distance(PlayerMovement.Instance.transform.position, enemy.transform.position) < Vector3.Distance(PlayerMovement.Instance.transform.position, targetEnemy.transform.position))
                    {
                        targetEnemy = enemy;
                    }
                }
            }

        }

        if (targetEnemy != null)
        {
            Vector3 dirToClosestEnemyPosition = (targetEnemy.transform.position - mainCamera.transform.position).normalized;
            closestEnemyPositionIndicator.eulerAngles = new Vector3(0, 0, UtilsClass.GetAngleFromVector(dirToClosestEnemyPosition));


            float distanceToClosestEnemyPosition = Vector3.Distance(targetEnemy.transform.position, PlayerMovement.Instance.transform.position); //Change this to not use Controller Instance
            closestEnemyPositionIndicator.gameObject.SetActive(Mathf.Abs(distanceToClosestEnemyPosition) > mainCamera.orthographicSize * 1.2f);
        }
        else
        {
            closestEnemyPositionIndicator.gameObject.SetActive(false);
        }


    }

    private void SetMessageText(string message)
    {
        waveMessageText.SetText(message);
    }

    private void SetWaveNumberText(string text)
    {
        waveNumberText.SetText(text);
    }
}
