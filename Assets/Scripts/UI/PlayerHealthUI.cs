using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    [SerializeField] private HealthSystem healthSystem;

    private RectTransform barRectTransform;

    private void Awake()
    {
        barRectTransform = transform.Find("bar").GetComponent<RectTransform>();
    }

    private void Start()
    {
        UpdateBar();
        //healthSystem.OnDamaged += HealthSystem_OnDamaged;
        //healthSystem.OnHealed += HealthSystem_OnHealed;
    }

    private void HealthSystem_OnHealed()
    {
        UpdateBar();
    }

    private void HealthSystem_OnDamaged()
    {
        UpdateBar();
    }

    public void UpdateBar()
    {
        barRectTransform.localScale = new Vector3(healthSystem.GetHealthAmountNormalize(), 1, 1);
    }
}
