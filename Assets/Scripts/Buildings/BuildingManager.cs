using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingManager : MonoBehaviour
{
    public event Action<BuildingTypeSO> OnActiveBuildingTypeChanged;
    public event Action OnShowBuildingSelectedUI;

    public static BuildingManager Instance { get; private set; }

    [SerializeField] BuildingTypeListSO buildingTypeList; //ChangeToList
    [SerializeField] BuildingTypeSO activeBuildingType;
    [SerializeField] LayerMask groundMask;
    [Tooltip("YOffset to mouse position")][SerializeField] Vector3 groundOffset = new Vector3(0, 2, 0);

    [SerializeField] private Building hqBuilding;


    private void Awake()
    {
        Instance = this;
        buildingTypeList = Resources.Load<BuildingTypeListSO>(typeof(BuildingTypeListSO).Name);
    }

    private void Start()
    {
        hqBuilding.GetComponent<HealthSystem>().OnDied += HQ_OnDied;
        WeaponManager.Instance.OnWeaponChanged += WeaponManager_OnWeaponChanged;
    }

    private void WeaponManager_OnWeaponChanged()
    {
        SetActiveBuildingType(null);
    }

    private void HQ_OnDied()
    {
        SoundManager.Instance.PlaySound(SoundManager.Sound.GameOver);
        EnemyWaveUI.Instance.Hide();
        GameOverUI.Instance.Show();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B) || Input.GetMouseButtonDown(1))
        {
            OnShowBuildingSelectedUI?.Invoke();
        }

        BuildingGhostMaterialColorChange();

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            if (activeBuildingType != null) 
            {
                if(CanSpawnBuilding(activeBuildingType, UtilsClass.GetMouseWorldPosition3D(), out string errorMessage))
                {
                    if (ResourceManager.Instance.CanAfford(activeBuildingType.constructionResourceCostArray))
                    {
                        ResourceManager.Instance.SpendResources(activeBuildingType.constructionResourceCostArray);
                        //Instantiate(activeBuildingType.prefab, UtilsClass.GetMouseWorldPosition3D(), Quaternion.identity);
                        BuildingConstruction.Create(UtilsClass.GetMouseWorldPosition3D(), activeBuildingType);
                        SoundManager.Instance.PlaySound(SoundManager.Sound.BuildingPlaced);
                    }
                    else
                    {
                        TooltipUI.Instance.Show("Cannot Afford " + activeBuildingType.GetConstructionResourceCostString(), new TooltipUI.TooltipTimer { timer = 2f });
                    }
                }
                else
                {
                    TooltipUI.Instance.Show(errorMessage, new TooltipUI.TooltipTimer { timer = 2f });
                }
               

            }
            //Debug.Log(CanSpawnBuilding(buildingTypeList.list[0], UtilsClass.GetMouseWorldPosition3D()));
        }


        //For Debugging
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    Vector3 enemySpawnPosition = UtilsClass.GetMouseWorldPosition3D() + UtilsClass.GetRandomDir();

        //    Enemy.Create(enemySpawnPosition);
        //}

    }

    private void BuildingGhostMaterialColorChange()
    {
        if (activeBuildingType != null)
        {
            if (CanSpawnBuilding(activeBuildingType, UtilsClass.GetMouseWorldPosition3D(), out string errorMessage))
            {
                BuildingGhost.Instance.BuildingGhostMaterialColor(Color.green);
            }
            else
            {
                BuildingGhost.Instance.BuildingGhostMaterialColor(Color.red);
            }
        }
    }

    public void SetActiveBuildingType(BuildingTypeSO buildingType)
    {
        activeBuildingType = buildingType;
        OnActiveBuildingTypeChanged?.Invoke(activeBuildingType);
    }

    public BuildingTypeSO GetActiveBuilingType()
    {
        return activeBuildingType;
    } 

    private bool CanSpawnBuilding(BuildingTypeSO buildingType, Vector3 position, out string errorMessage)
    {
        BoxCollider boxCollider = buildingType.prefab.GetComponent<BoxCollider>();

        Collider[] colliderArray = Physics.OverlapBox(position + groundOffset, boxCollider.size/2);

        bool isAreaClear = colliderArray.Length == 0;
        if (!isAreaClear)
        {
            errorMessage = "Area is not clear!";
            return false;
        }

        colliderArray = Physics.OverlapSphere(position, buildingType.minConstructionRadius);

        foreach (Collider collider in colliderArray)
        {
            //Collider inside the constructionRadius;
            BuildingTypeHolder buildingTypeHolder = collider.GetComponent<BuildingTypeHolder>();
            if (buildingTypeHolder != null)
            {
                //Has BuildingTypeHolder
                if(buildingTypeHolder.buildingType == buildingType)
                {
                    //There is already a building of this type within the radius
                    errorMessage = "Nearby a building of the same type!";
                    return false;
                }
            }

        }

        float maxConstructionRadius = 25f;
        colliderArray = Physics.OverlapSphere(position, maxConstructionRadius);

        foreach (Collider collider in colliderArray)
        {
            //Collider inside the constructionRadius;
            BuildingTypeHolder buildingTypeHolder = collider.GetComponent<BuildingTypeHolder>();
            if (buildingTypeHolder != null)
            {
                errorMessage = "";
                return true;
            }

        }

        errorMessage = "Too far away from other buildings";
        return false;
    }

    public Building GetHQBuilding()
    {
        if (hqBuilding == null)
            return null;

        return hqBuilding;
    }

}
