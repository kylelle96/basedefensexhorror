using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour, IDamagable
{
    private HealthSystem healthSystem;
    private BuildingTypeSO buildingType;
    private Transform buildingDemolishButton;
    private Transform buildingRepairButton;

    private void Awake()
    {
        healthSystem = GetComponent<HealthSystem>();
        buildingType = GetComponent<BuildingTypeHolder>().buildingType;
        buildingDemolishButton = transform.Find("pfBuildingDemolishButton");
        buildingRepairButton = transform.Find("pfBuildingRepairButton");
        HideBuildingDemolishButton();
        HideBuildingRepairButton();
    }

    // Start is called before the first frame update
    void Start()
    {
       
        healthSystem.SetHealthAmountMax(buildingType.healthAmountMax, true);
        //healthSystem.OnDamaged += HealthSystem_OnDamaged;
        //healthSystem.OnHealed += HealthSystem_OnHealed;
        //healthSystem.OnDied += HealthSystem_OnDied;
        healthSystem.OnDamagedEvent.AddListener(HealthSystem_OnDamaged);
        healthSystem.OnHealedEvent.AddListener(HealthSystem_OnHealed);
        healthSystem.OnDiedEvent.AddListener(HealthSystem_OnDied);
    }

    private void HealthSystem_OnHealed()
    {
        if (healthSystem.IsFullHealth())
        {
            HideBuildingRepairButton();
        }
    }

    private void HealthSystem_OnDamaged()
    {
        ShowBuildingRepairButton();
        //SoundManager.Instance.PlaySound(SoundManager.Sound.BuildingDamaged);
    }

    private void ShowBuildingDemolishButton()
    {
        if (buildingDemolishButton != null)
            buildingDemolishButton.gameObject.SetActive(true);
    }

    private void HideBuildingDemolishButton()
    {
        if (buildingDemolishButton != null)
            buildingDemolishButton.gameObject.SetActive(false);
    }

    private void ShowBuildingRepairButton()
    {
        if (buildingRepairButton != null)
            buildingRepairButton.gameObject.SetActive(true);
    }

    private void HideBuildingRepairButton()
    {
        if (buildingRepairButton != null)
            buildingRepairButton.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        //healthSystem.OnDied -= HealthSystem_OnDied;
        healthSystem.OnDamagedEvent.RemoveListener(HealthSystem_OnDamaged);
        healthSystem.OnHealedEvent.RemoveListener(HealthSystem_OnHealed);
        healthSystem.OnDiedEvent.RemoveListener(HealthSystem_OnDied);
    }

    private void HealthSystem_OnDied()
    {
        //SoundManager.Instance.PlaySound(SoundManager.Sound.BuildingDestroyed);
        DeathImpact deathImpact = buildingType.particleDeathImpact.deathImpact.Get<DeathImpact>(transform.position, Quaternion.identity);
        deathImpact.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject);
    }

    private void OnMouseEnter()
    {
        ShowBuildingDemolishButton();
    }

    private void OnMouseExit()
    {
        HideBuildingDemolishButton();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
