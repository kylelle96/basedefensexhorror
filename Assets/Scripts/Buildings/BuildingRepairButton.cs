using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingRepairButton : MonoBehaviour
{
    [SerializeField] private HealthSystem healthSystem;
    [SerializeField] List<ResourceTypeSO> resourceTypesToRepairList;
    private List<ResourceAmount> resourceAmountList = new List<ResourceAmount>();

    private void Awake()
    {
        transform.Find("button").GetComponent<Button>().onClick.AddListener(() =>
        {
            int missingHealth = healthSystem.GetHealthAmountMax() - healthSystem.GetHealthAmount();
            int repairCost = missingHealth /resourceTypesToRepairList.Count;

            foreach(ResourceTypeSO resourceType in resourceTypesToRepairList)
            {
                resourceAmountList.Add(new ResourceAmount { resourceType = resourceType, amount = repairCost });
            }

            if (ResourceManager.Instance.CanAfford(resourceAmountList.ToArray()))
            {
                ResourceManager.Instance.SpendResources(resourceAmountList.ToArray());
                healthSystem.HealFull();
                SoundManager.Instance.PlaySound(SoundManager.Sound.BuildingPlaced);
            }
            else
            {
                //Cannot Afford;
            }

        });
    }
}