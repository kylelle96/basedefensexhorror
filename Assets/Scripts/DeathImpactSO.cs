using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Death Impact")]
public class DeathImpactSO : ScriptableObject
{
    public DeathImpact deathImpact;
}
